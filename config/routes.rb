Rails.application.routes.draw do
  resources :users do
    collection do
      get :count
    end
    member do
      get :name
      resources :shop
    end
  end
end
