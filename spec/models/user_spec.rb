require "rails_helper"

RSpec.describe User, :type => :model do
    context "user property validation" do
        it "user has empty first name" do 
            user = User.new(last_name: "Lin", age: 33)
            expect(user).to be_invalid 
        end

        it "user has empty last name" do 
            user = User.new(first_name: "David", age: 33)
            expect(user).to be_invalid 
        end

        it "user has incorrect type of age" do 
            user = User.new(first_name: "David", last_name: "Lin", age: "error type")
            expect(user).to be_invalid 
        end

        it "user has valid parameters" do 
            user = User.new(first_name: "David", last_name: "Lin", age: 33, gender: "male", address: [county: "test", address_1: "xxx rd.", address_2: "no. 33"])
            expect(user).to be_valid 
        end
    end
end