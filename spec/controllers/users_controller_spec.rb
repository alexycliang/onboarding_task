require "rails_helper"

describe UsersController do
    before(:all) do 
        @user = User.create
    end 
    
    after(:all) do
        @user = User.last
        @user.destroy
    end     
    
    describe "GET /users" do 
        it "has a 200 status code" do 
            get :index
            expect(response).to have_http_status(200) 
        end
    end 

    describe "POST /users" do 
        context "with valid parameter" do
            let(:user_params) do 
                {   
                    user: {
                        first_name: "Hung-Chi",
                        last_name: "Kuo",
                        age: 37,
                        gender: "male",
                        address_attributes: {
                            county: "Tainan",
                            address_1: "XX rd.",
                            address_2: "No. 22"
                        }                            
                    }
                }
            end
        
            it "create a new user with empty first name" do
                user_params[:user][:first_name] = ""
                post :create, user_params
                expect(response).to render_template(:new) 
            end

            it "create a new user with valid parameter" do
                post :create, user_params
                expect(User.where(first_name: user_params[:user][:first_name]).count).to eq(1)
                expect(response).to have_http_status(302) 
            end
        end
    end  
end