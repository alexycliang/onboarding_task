json.first_name @user.first_name
json.last_name @user.last_name
json.age @user.age
json.gender @user.gender
json.county @user.address.county
json.address1 @user.address.address_1
json.address2 @user.address.address_2