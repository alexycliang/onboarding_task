var app = angular.module("onboarding-task");

app.directive("userForm", function() {
  return {
    templateUrl: "/templates/user-form.html"
  };
});
