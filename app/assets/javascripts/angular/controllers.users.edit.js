var app = angular.module("onboarding-task", []);

var TARGET_URL = "/users/";

// Add CSRF token to API request header
app.config([
  "$httpProvider",
  function($httpProvider) {
    $httpProvider.defaults.headers.common["X-CSRF-Token"] = $(
      "meta[name=csrf-token]"
    ).attr("content");
  }
]);

app.controller("userEditController", [
  "$scope",
  "$location",
  "$window",
  "UserService",
  function($scope, $location, $window, UserService) {
    // Controller methods
    $scope.createUser = createUser;
    $scope.getUser = getUser;
    $scope.updateUser = updateUser;

    // Declare user object for ng-model
    $scope.user = {};

    function createUser() {
      UserService.createUser($scope.user)
        .then(function() {
          $window.location.href = TARGET_URL;
        })
        .catch(function(error) {
          console.error(error);
        });
    }

    function getUser(userId) {
      UserService.getUser(userId).then(function(response) {
        $scope.user = {
          firstName: response.data.first_name,
          lastName: response.data.last_name,
          age: response.data.age,
          gender: response.data.gender,
          address: {
            county: response.data.county,
            address1: response.data.address1,
            address2: response.data.address2
          }
        };
      });
    }

    function updateUser(userId, user) {
      UserService.updateUser(userId, user)
        .then(function() {
          $window.location.href = TARGET_URL + userId;
        })
        .catch(function(error) {
          console.info(error);
        });
    }
  }
]);
