var app = angular.module("onboarding-task");

app.service("UserService", function($http) {
  var TARGET_URL = "/users/";

  var service = {
    createUser: createUser,
    getUser: getUser,
    updateUser: updateUser
  };
  return service;

  function createUser(userObj) {
    return $http({
      method: "POST",
      url: TARGET_URL,
      headers: {
        "Content-Type": "application/json"
      },
      data: generateDataPayload(userObj)
    });
  }

  function getUser(userId) {
    return $http({
      method: "GET",
      url: TARGET_URL + userId + ".json"
    });
  }

  function updateUser(userId, userObj) {
    return $http({
      method: "PATCH",
      url: TARGET_URL + userId + ".json",
      headers: {
        "Content-Type": "application/json"
      },
      data: generateDataPayload(userObj)
    });
  }

  function generateDataPayload(userObj) {
    return {
      user: {
        first_name: userObj.firstName,
        last_name: userObj.lastName,
        age: userObj.age,
        gender: userObj.gender,
        address_attributes: {
          county: userObj.address.county,
          address_1: userObj.address.address1,
          address_2: userObj.address.address2
        }
      }
    };
  }
});
