class User
	include Mongoid::Document

	field :first_name, type: String
	field :last_name, type: String
	field :age, type: Integer
	field :gender, type: String

	validates :first_name, presence: true
	validates :last_name, presence: true
	validates_numericality_of :age, :only_integer => true

	has_one :address, dependent: :destroy 
	has_one :shop, dependent: :destroy
	accepts_nested_attributes_for :address
end
