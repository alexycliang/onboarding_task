class Address
	include Mongoid::Document
	
	field :county, type: String
	field :address_1, type: String
	field :address_2, type: String
	field :user_id, type: Integer

	belongs_to :user
end
